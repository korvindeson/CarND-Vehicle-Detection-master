from helper import extract_features,get_hog_features
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC

from sklearn.model_selection import GridSearchCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt
import os
import numpy as np
import cv2
import glob



# Print everything, run analysis
# verbous_mode = 1
# Debug mode
# verbous_mode = 0
verbous_mode = 1


def get_pics_from_folder(folder='./', lab=1, imread=0):
    lis = os.listdir(folder)
    
    vehicles=[]
    for l in lis:
        lis2 = os.listdir(folder + l)
        for ll in lis2:
            if imread == 0: 
                vehicles.append((folder + '/' + l + '/' + ll).replace('//','/'))
            else:
                vehicles.append(cv2.imread((folder + '/' + l + '/' + ll).replace('//','/')))
                vehicles[-1] = cv2.cvtColor(vehicles[-1], cv2.COLOR_BGR2RGB)
    if lab == 1:
        labels = np.ones(len(vehicles))
    elif lab == 0:
        labels = np.zeros(len(vehicles))
    else:
        labels = np.ones(len(vehicles))
        labels *= lab
    return vehicles, labels

def get_features_with_labels():
    # Get picture paths
        # labels = 1
    vehicles,     v_labels  = get_pics_from_folder(folder='./data/vehicles/',lab=1,imread=0)
        # labels = 0
    non_vehicles, nv_labels = get_pics_from_folder(folder='./data/non-vehicles/',lab=0,imread=0)
    
    
    #vehicles = glob.glob('./data/vehicles/*/*')
    #non_vehicles = glob.glob('./data/non-vehicles/*/*')
    
    #print(vehicles)
    
    '''
    # Visualize one hog feature
    
    orient=10
    pix_per_cell=16
    cell_per_block=2
    
    veh = plt.imread(vehicles[0])
    veh = cv2.cvtColor(veh, cv2.COLOR_RGB2YCrCb)
    arr=[]
    for channel in range(3):
        f,i = get_hog_features(veh[:,:,channel], orient, pix_per_cell, cell_per_block, 
                                            vis=True, feature_vec=True)
        arr.append(i)
    
    print(np.shape(arr[0]))
    
    plt.imshow(veh)
    plt.show()
    plt.imshow(arr[2])
    plt.show()
    '''
    
    
    # combine into dataset
    dataset = np.vstack((vehicles,non_vehicles))
    #dataset = np.concatenate((vehicles,non_vehicles),0)
    
    
    labels  = np.concatenate((v_labels,nv_labels),0)
    
    print('Dataset ', np.shape(dataset))
    print('Labels ',np.shape(labels))
    
    spatial_size=(32,32)
    
    color_space      = 'YCrCb'
    color_space_add  = 'YCrCb'
    orient           = 10
    pix_per_cell     = 16
    
    # Set to true to add feature
    add_feature = True
    
    # Extract features
    if add_feature:
        ii=0
        c_dataset = dataset.copy()
        for frame in dataset:
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            edges = cv2.Canny(gray, 0, 250)
            
            sharp=c_dataset[ii].copy()
            sharp[:,:,0] = gray
            sharp[:,:,1] = gray
            sharp[:,:,2] = gray
            
            c_dataset[ii]=cv2.addWeighted(c_dataset[ii], 1.5, sharp, -0.5, 0)
            
            ii += 1
        print(np.min(c_dataset[0]))
        print(np.max(c_dataset[0]))
        plt.imshow(c_dataset[0])
        plt.show()
        plt.imshow(gray)
        plt.show()
        add_features = extract_features(c_dataset, color_space=color_space_add, spatial_size=spatial_size,
                                      hist_bins=32, orient=orient, 
                                      pix_per_cell=pix_per_cell, cell_per_block=2, hog_channel='ALL',
                                      spatial_feat=True, hist_feat=True, hog_feat=True)
    
    features = extract_features(dataset, color_space=color_space, spatial_size=spatial_size,
                                  hist_bins=32, orient=orient, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    
    #print(np.shape(features))
    #print(np.shape(add_features))
    c_features = features.copy()
    if add_feature:
        add_features = np.concatenate((features,add_features),1)
        
    
    # Normalize
    features     = MinMaxScaler().fit_transform(features)
    add_features = MinMaxScaler().fit_transform(add_features)
    c_features   = MinMaxScaler().fit_transform(c_features)
    
    #features = PCA(n_components=3).fit_transform(features)
    
    
    print('Features ', np.shape(features))
    print('Labels '  , np.shape(labels))
    
    return c_features, labels, add_features

if verbous_mode == 1:
    # Get features    
    features, labels = get_features_with_labels()        
    # Train-Test split with 30% testing
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.30, random_state=42)


# Block from my previous project
# https://github.com/korvindeson/POI_Enron

### Evaluate different classfiers

#dict to store results of classifiers
i={}
        

#initialize classifiers
lr = LogisticRegression()
gnb = GaussianNB()
svc = SVC()


### gridsearch will be used, results of grid search against manual setting will
### be interesting
parameters = {'max_features': ['sqrt', 'log2'],'min_samples_split':[7,8,9,10]}
dctG = GridSearchCV(DecisionTreeClassifier(criterion="entropy", random_state=42),parameters)
dct = DecisionTreeClassifier(criterion="entropy", max_features="log2", random_state=42,min_samples_split=10)

### same for Random Forest
parameters = {'n_estimators': [8,9,10]}
rfcG = GridSearchCV(RandomForestClassifier(), parameters)
rfc = RandomForestClassifier(max_features='log2',n_estimators=3)


### function to iterate through classifiers with different features
def check_classifiers(features,labels,sss):

    for train_index, test_index in sss.split(features, labels):
        
        y_train=[]
        X_train=[]
        y_test=[]
        X_test=[]
        
        for e in train_index:
            y_train.append(labels[e])
            X_train.append(features[e])
        for e in test_index:
            y_test.append(labels[e])
            X_test.append(features[e])
        
        
        
        ### loop over classifiers
        for clf, name in [(lr, 'Logistic'),
                          (gnb, 'Naive Bayes'),
                          (svc, 'Support Vector Classification'),
                          (rfcG, 'Random Forest - GridSearch'),
                          (rfc, 'Random Forest'),
                          (dctG, 'Decision Tree - GridSearch'),
                          (dct, 'Decision Tree')]:
            #fit training data
            clf.fit(X_train, y_train)
            
            #predict testing data
            pred=clf.predict(X_test)
           
            #initialize variable
            total_predictions=0
            true_positives=0
            false_positives=0
            false_negatives=0
            true_negatives=0
    
     
            #loop over predictions and labels
            #
            for p,y in zip(pred,y_test):
                if p == 0 and y == 0:
                    true_negatives += 1
                elif p == 0 and y == 1:
                    false_negatives += 1
                elif p == 1 and y == 0:
                    false_positives += 1
                elif p == 1 and y == 1:
                    true_positives += 1
                else:
                    print("O_o") #Just in case
            
                  ### took this from test.py it is just a formulas
            total_predictions = total_predictions + true_negatives + false_negatives + false_positives + true_positives
            accuracy = 1.0*(true_positives + true_negatives)/total_predictions
            if true_positives+false_positives == 0:
                precision = 0
            else:
                precision = 1.0*true_positives/(true_positives+false_positives)
            recall = 1.0*true_positives/(true_positives+false_negatives)
            f1 = 2.0 * true_positives/(2*true_positives + false_positives+false_negatives)
            if (4*precision + recall) == 0:
                f2 = 0
            else:
                f2 = (1+2.0*2.0) * precision*recall/(4*precision + recall)
            
            #store results, average new ones
            if name in i:
                i[name] = { 'accuracy'    : (i[name]['accuracy']+accuracy)/2,
                            'precision'   : (i[name]['precision']+precision)/2,
                            'recall'      : (i[name]['recall']+recall)/2,
                            'f1'          : (i[name]['f1']+f1)/2,
                            'f2'          : (i[name]['f2']+f2)/2               }
            else:
                i[name] = { 'accuracy'    : accuracy,
                            'precision'   : precision,
                            'recall'      : recall,
                            'f1'          : f1,
                            'f2'          : f2       }
    
    return i


PERF_FORMAT_STRING = "\
\tAccuracy: {:>0.{display_precision}f}\tPrecision: {:>0.{display_precision}f}\t\
Recall: {:>0.{display_precision}f}\tF1: {:>0.{display_precision}f}\tF2: {:>0.{display_precision}f}"


if verbous_mode == 1:
    # uncomment to run analysis of different ML models on dataset
    
    # go over test 50 times, split test/train 50%/50%, shuffle order of numbers inside
    sss = StratifiedShuffleSplit(n_splits=1, test_size=0.5, random_state=42)
    backup_i= check_classifiers(features,labels,sss)
    for c in i:
        print (c, '\n', PERF_FORMAT_STRING.format(i[c]['accuracy'], i[c]['precision'], 
                                                  i[c]['recall'], i[c]['f1'], i[c]['f2'], display_precision = 5))



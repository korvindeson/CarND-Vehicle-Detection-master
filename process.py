from helper import slide_window,draw_boxes,draw_boxes_with_predict,extract_features,add_heat,apply_threshold,draw_labeled_bboxes
from sklearn.externals import joblib
from moviepy.editor import VideoFileClip
import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler,MinMaxScaler

from scipy.ndimage.measurements import label

from PIL import Image
from resizeimage import resizeimage
import cv2



global hist_pred_64
global hist_pred_128
global hist_pred_160
global heatmap



def process(image):
    global heatmap
    # Analyze Top side of the image
    windows_64 = slide_window(image, x_start_stop=[100, None], y_start_stop=[320, 500], 
                          xy_window=(64, 64), xy_overlap=(0.8, 0.8))

    arr_64 = draw_boxes(image, windows_64, color=(0, 0, 255), thick=6, only_arr=True)
    
    # Analyze Bottom side of the image      
    windows_128 = slide_window(image, x_start_stop=[None, None], y_start_stop=[300, 550], 
                               xy_window=(128, 128), xy_overlap=(0.8, 0.8))
    
    arr_128 = draw_boxes(image, windows_128, color=(0, 0, 255), thick=6, only_arr=True)
    
    crop_size = (64,64)
    
    for i in range(len(arr_128)):        
        arr_128[i] = cv2.resize(arr_128[i], crop_size, interpolation = cv2.INTER_CUBIC)
        #arr_128[i] = cv2.cvtColor(arr_128[i], cv2.COLOR_BGR2RGB)
    # Analyze Bottom side of the image      
    windows_160 = slide_window(image, x_start_stop=[None, None], y_start_stop=[400, 650], 
                               xy_window=(160, 160), xy_overlap=(0.8, 0.8))    
    
    arr_160 = draw_boxes(image, windows_160, color=(0, 0, 255), thick=6, only_arr=True)
        
    for i in range(len(arr_160)):
        arr_160[i] = cv2.resize(arr_160[i], crop_size, interpolation = cv2.INTER_CUBIC)
        #arr_160[i] = cv2.cvtColor(arr_160[i], cv2.COLOR_BGR2RGB)
    
    
    spatial_size     = (32,32)
    color_space      = 'YCrCb'
    color_space_add  = 'YCrCb'
    orient           = 10
    pix_per_cell     = 16
    
    
    # Set to true to add feature
    add_feature = True
    
    #features_64 = extract_features(arr, color_space='RGB', spatial_size=spatial_size,
    #                              hist_bins=32, orient=9, 
    #                              pix_per_cell=8, cell_per_block=2, hog_channel=0,
    #                              spatial_feat=True, hist_feat=True, hog_feat=True)
    if add_feature:
        ii=0
        c_dataset = arr_64.copy()
        for frame in c_dataset:
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            edges = cv2.Canny(gray, 0, 250)
            
            sharp=c_dataset[ii].copy()
            sharp[:,:,0] = gray
            sharp[:,:,1] = gray
            sharp[:,:,2] = gray
            
            c_dataset[ii]=cv2.addWeighted(c_dataset[ii], 1.5, sharp, -0.5, 0)
            
            ii += 1
        #plt.imshow(c_dataset[0])
        #plt.show()
        
        add_features_64 = extract_features(c_dataset, color_space=color_space_add, spatial_size=spatial_size,
                                          hist_bins=32, orient=orient, 
                                          pix_per_cell=pix_per_cell, cell_per_block=2, hog_channel='ALL',
                                          spatial_feat=True, hist_feat=True, hog_feat=True)
            
    features_64 = extract_features(arr_64, color_space=color_space, spatial_size=spatial_size,
                                  hist_bins=32, orient=orient, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    
    
    
    if add_feature:
        ii=0
        c_dataset = arr_128.copy()
        for frame in c_dataset:
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            edges = cv2.Canny(gray, 0, 250)
            
            sharp=c_dataset[ii].copy()
            sharp[:,:,0] = gray
            sharp[:,:,1] = gray
            sharp[:,:,2] = gray
            
            c_dataset[ii]=cv2.addWeighted(c_dataset[ii], 1.5, sharp, -0.5, 0)
            
            ii += 1
        #plt.imshow(c_dataset[0])
        #plt.show()
        
        add_features_128 = extract_features(c_dataset, color_space=color_space_add, spatial_size=spatial_size,
                                          hist_bins=32, orient=orient, 
                                          pix_per_cell=pix_per_cell, cell_per_block=2, hog_channel='ALL',
                                          spatial_feat=True, hist_feat=True, hog_feat=True)
            
    features_128 = extract_features(arr_128, color_space=color_space, spatial_size=spatial_size,
                                  hist_bins=32, orient=orient, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    
    if add_feature:
        ii=0
        c_dataset = arr_160.copy()
        for frame in c_dataset:
            gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            edges = cv2.Canny(gray, 0, 250)
            
            sharp=c_dataset[ii].copy()
            sharp[:,:,0] = gray
            sharp[:,:,1] = gray
            sharp[:,:,2] = gray
            
            c_dataset[ii]=cv2.addWeighted(c_dataset[ii], 1.5, sharp, -0.5, 0)
            
            ii += 1
        add_features_160 = extract_features(c_dataset, color_space=color_space_add, spatial_size=spatial_size,
                                          hist_bins=32, orient=orient, 
                                          pix_per_cell=pix_per_cell, cell_per_block=2, hog_channel='ALL',
                                          spatial_feat=True, hist_feat=True, hog_feat=True)
            
    features_160 = extract_features(arr_160, color_space=color_space, spatial_size=spatial_size,
                                  hist_bins=32, orient=orient, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    
    if add_feature:
        add_features_64 = np.concatenate((features_64,add_features_64),1)
        add_features_128 = np.concatenate((features_128,add_features_128),1)
        add_features_160 = np.concatenate((features_160,add_features_160),1)
        
    
    features_64  = MinMaxScaler().fit_transform(features_64)
    add_features_64  = MinMaxScaler().fit_transform(add_features_64)
    
    features_128 = MinMaxScaler().fit_transform(features_128)
    add_features_128 = MinMaxScaler().fit_transform(add_features_128)
    
    features_160 = MinMaxScaler().fit_transform(features_160)
    add_features_160 = MinMaxScaler().fit_transform(add_features_160)
    
    #features_128 = PCA(n_components=3).fit_transform(features_128)
    #features_160 = PCA(n_components=3).fit_transform(features_160)
    
    f_pred_64  = clf.predict(add_features_64)
    f_pred_128 = clf.predict(add_features_128)
    f_pred_160 = clf.predict(add_features_160)
    
    s_pred_64  = svc.predict(features_64)
    s_pred_128 = svc.predict(features_128)
    s_pred_160 = svc.predict(features_160)
    
    s_ind_64  = np.where(s_pred_64 == 1)[0]
    s_ind_128 = np.where(s_pred_128 == 1)[0]
    s_ind_160 = np.where(s_pred_160 == 1)[0]
    
    f_ind_64 = np.where(f_pred_64 == 1)[0]
    f_ind_128 = np.where(f_pred_128 == 1)[0]
    f_ind_160 = np.where(f_pred_160 == 1)[0]
    
    int_64  = np.intersect1d(s_ind_64,f_ind_64).tolist()
    int_128 = np.intersect1d(s_ind_128,f_ind_128).tolist()
    int_160 = np.intersect1d(s_ind_160,f_ind_160).tolist()
    
    pred_64 = np.zeros_like(s_pred_64,dtype=np.float32)
    pred_64[int_64]=1
    
    pred_128 = np.zeros_like(s_pred_128,dtype=np.float32)
    pred_128[int_128]=1
    
    pred_160 = np.zeros_like(s_pred_160,dtype=np.float32)
    pred_160[int_160]=1
    
    
    
    #hist_pred_64  = np.append((hist_pred_64,pred_64),1)
    #hist_pred_128 = hist_pred_128.append(pred_128)
    #hist_pred_160 = hist_pred_160.append(pred_160)
    
    #print(hist_pred_64)
    
    
    
    for i in range(len(hist_pred_64)):
        hist_pred_64[i] = np.average((hist_pred_64[i],pred_64[i]))
        if hist_pred_64[i] < 0.25:
            hist_pred_64[i] = 0.
        elif hist_pred_64[i] > 0.6:
            hist_pred_64[i] = 1.
            
    for i in range(len(hist_pred_128)):
        hist_pred_128[i]= np.average((hist_pred_128[i],pred_128[i]))
        if hist_pred_128[i] < 0.25 and hist_pred_128[i] > 0.:
            hist_pred_128[i] = 0
        elif hist_pred_128[i] > 0.6:
            hist_pred_128[i] = 1.
            
    for i in range(len(hist_pred_160)):
        hist_pred_160[i]= np.average((hist_pred_160[i],pred_160[i]))
        if hist_pred_160[i] < 0.25 and hist_pred_160[i] > 0.:
            hist_pred_160[i] = 0
        elif hist_pred_160[i] > 0.6:
            hist_pred_160[i] = 1.
    
    
    #heatmap = np.zeros_like(image)
    
    if int_64 != []:
        heatmap = add_heat(heatmap,windows_64, int_64)
    if int_128 != []:
        heatmap = add_heat(heatmap,windows_128,int_128)
    if int_160 != []:
        heatmap = add_heat(heatmap,windows_160,int_160)
    
    #plt.imshow(heatmap)
    #plt.show()
    
    heatmap = apply_threshold(heatmap,20)
    
    plt.imshow(heatmap)
    plt.show()
    
    labels = label(heatmap)
    
    new_image = draw_labeled_bboxes(image,labels)
    
    #new_image = draw_boxes_with_predict(image, hist_pred_64, windows_64, color=(0, 0, 255), thick=6)
    #new_image = draw_boxes_with_predict(new_image, hist_pred_128, windows_128, color=(0, 0, 255), thick=6)
    #new_image = draw_boxes_with_predict(new_image, hist_pred_160, windows_160, color=(0, 0, 255), thick=6)
    heatmap= np.multiply(heatmap,0.5)
    plt.imshow(new_image)
    plt.show()
    return new_image



if __name__ == "__main__":
    
    
    
    
    image = cv2.imread('./test_images/test1.jpg')
    
    windows_64 = slide_window(image, x_start_stop=[100, None], y_start_stop=[320, 450], 
                              xy_window=(64, 64), xy_overlap=(0.8, 0.8))
    
    windows_128 = slide_window(image, x_start_stop=[None, None], y_start_stop=[300, 550], 
                               xy_window=(128, 128), xy_overlap=(0.8, 0.8))
        
    windows_160 = slide_window(image, x_start_stop=[None, None], y_start_stop=[400, 650], 
                               xy_window=(160, 160), xy_overlap=(0.8, 0.8))
    
    heatmap = np.zeros_like(image)
    
    hist_pred_64  = np.zeros(len(windows_64))
    hist_pred_128 = np.zeros(len(windows_128))
    hist_pred_160 = np.zeros(len(windows_160))
    
    clf = joblib.load('./classifier/forest.pkl') 
    svc = joblib.load('./classifier/svc.pkl') 
    
    
    clip1 = VideoFileClip("test_video.mp4")
    
    white_clip = clip1.fl_image(process)

    white_clip.write_videofile('processed_test_video.mp4', audio=False)
    
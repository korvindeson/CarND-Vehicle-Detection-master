from helper import slide_window,draw_boxes,draw_boxes_with_predict,extract_features
from sklearn.externals import joblib
from moviepy.editor import VideoFileClip
import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler,MinMaxScaler

from PIL import Image
from resizeimage import resizeimage
import cv2

def process(image):
    # Analyze Top side of the image
    #windows = slide_window(image, x_start_stop=[None, None], y_start_stop=[350, 500], 
    #                    xy_window=(64, 64), xy_overlap=(0.2, 0.2))
          
    #arr = draw_boxes(image, windows, color=(0, 0, 255), thick=6)                    
    
    # Analyze Bottom side of the image      
    windows_128 = slide_window(image, x_start_stop=[100, None], y_start_stop=[400, 550], 
                        xy_window=(128, 128), xy_overlap=(0.5, 0.5))
    
    arr_128 = draw_boxes(image, windows_128, color=(0, 0, 255), thick=6, only_arr=True)
    
    crop_size = (64,64)
    
    for i in range(len(arr_128)):        
        arr_128[i] = cv2.resize(arr_128[i], crop_size, interpolation = cv2.INTER_CUBIC)
        #arr_128[i] = cv2.cvtColor(arr_128[i], cv2.COLOR_BGR2RGB)
    # Analyze Bottom side of the image      
    windows_160 = slide_window(image, x_start_stop=[None, None], y_start_stop=[400, 650], 
                        xy_window=(160, 160), xy_overlap=(0.5, 0.5))
    
    arr_160 = draw_boxes(image, windows_160, color=(0, 0, 255), thick=6, only_arr=True)
        
    for i in range(len(arr_160)):
        arr_160[i] = cv2.resize(arr_160[i], crop_size, interpolation = cv2.INTER_CUBIC)
        #arr_160[i] = cv2.cvtColor(arr_160[i], cv2.COLOR_BGR2RGB)
    
    
    spatial_size=(8,8)
    
    #features_64 = extract_features(arr, color_space='RGB', spatial_size=spatial_size,
    #                              hist_bins=32, orient=9, 
    #                              pix_per_cell=8, cell_per_block=2, hog_channel=0,
    #                              spatial_feat=True, hist_feat=True, hog_feat=True)
    '''
    add_features_128 = extract_features(arr_128, color_space='YCrCb', spatial_size=spatial_size,
                                  hist_bins=32, orient=9, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    '''
    features_128 = extract_features(arr_128, color_space='YCrCb', spatial_size=spatial_size,
                                  hist_bins=32, orient=9, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    
    #features_128 = np.concatenate((features_128,add_features_128),1)
    
    '''
    add_features_160 = extract_features(arr_160, color_space='YCrCb', spatial_size=spatial_size,
                                  hist_bins=32, orient=9, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    '''
    features_160 = extract_features(arr_160, color_space='YCrCb', spatial_size=spatial_size,
                                  hist_bins=32, orient=9, 
                                  pix_per_cell=8, cell_per_block=2, hog_channel='ALL',
                                  spatial_feat=True, hist_feat=True, hog_feat=True)
    
    #features_160 = np.concatenate((features_160,add_features_160),1)
    
    #features_64  = MinMaxScaler().fit_transform(features_64)
    features_128 = MinMaxScaler().fit_transform(features_128)
    features_160 = MinMaxScaler().fit_transform(features_160)
    
    #features = PCA(n_components=3).fit_transform(features)
    
    #pred_64  = clf.predict(features_64)
    f_pred_128 = clf.predict(features_128)
    f_pred_160 = clf.predict(features_160)
    
    s_pred_128 = svc.predict(features_128)
    s_pred_160 = svc.predict(features_160)
    
    s_ind_128 = np.where(s_pred_128 == 1)[0]
    s_ind_160 = np.where(s_pred_160 == 1)[0]
    
    f_ind_128 = np.where(f_pred_128 == 1)[0]
    f_ind_160 = np.where(f_pred_160 == 1)[0]
    
    int_128 = np.intersect1d(s_ind_128,f_ind_128)
    int_160 = np.intersect1d(s_ind_160,f_ind_160)
    
    pred_128 = np.zeros_like(s_pred_128,dtype=np.int32)
    pred_128[int_128]=1
    
    pred_160 = np.zeros_like(s_pred_160,dtype=np.int32)
    pred_160[int_160]=1
    
    
    #new_image = draw_boxes_with_predict(image, pred_64, windows, color=(0, 0, 255), thick=6)
    new_image = draw_boxes_with_predict(image, pred_128, windows_128, color=(0, 0, 255), thick=6)
    new_image = draw_boxes_with_predict(new_image, pred_160, windows_160, color=(0, 0, 255), thick=6)
    
    plt.imshow(new_image)
    plt.show()
    return new_image


clf = joblib.load('./classifier/forest.pkl') 
svc = joblib.load('./classifier/svc.pkl') 

clip1 = VideoFileClip("test_video.mp4")
white_clip = clip1.fl_image(process)

white_clip.write_videofile('processed_test_video.mp4', audio=False)
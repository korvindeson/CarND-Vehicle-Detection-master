from main import get_pics_from_folder

from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import numpy as np
import cv2

dk = ImageDataGenerator(rotation_range=15,
                        width_shift_range=0.3,
                        height_shift_range=0.3,
                        shear_range=0.1,
                        zoom_range=0,
                        fill_mode='nearest',
                        horizontal_flip=True,
                        vertical_flip=False)

f, l = get_pics_from_folder(folder='./data/vehicles/',lab=0,imread=1)


train_x, train_y=[],[]
ii=0

f=np.array(f)
l=np.array(l)

dk.fit(f)

for i,j in dk.flow( f , l , batch_size=1):
    
    ii+=1
    
    cv2.imwrite('./data/vehicles/keras/{}.png'.format(ii),i[0])
    if ii >= 10000:
        break
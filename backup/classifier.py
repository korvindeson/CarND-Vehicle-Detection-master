from main import get_features_with_labels
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
from sklearn.svm import LinearSVC

from sklearn.decomposition import PCA



import numpy as np

def train_classifier():

    parameters = {'n_estimators': [7,15,50,100]}   #, 'max_depth': [50,100,200], 'min_samples_leaf': [1,2,5]} 
    clf = GridSearchCV(RandomForestClassifier(n_jobs=5,random_state=42), parameters)
    #clf = RandomForestClassifier(n_jobs=5,random_state=42,n_estimators=200)
    features, labels = get_features_with_labels()
    
    # Test split - 15%
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.15, random_state=42)
    print(np.shape(X_train))
    
    #X_train = PCA(n_components=1).fit_transform(X_train)
    #X_test = PCA(n_components=1).fit_transform(X_test)
    
    clf.fit(X_train, y_train)
    
    #predict testing data
    pred=clf.predict(X_test)
       
    #initialize variable
    total_predictions=0
    true_positives=0
    false_positives=0
    false_negatives=0
    true_negatives=0
    
     
    #loop over predictions and labels
    #
    for p,y in zip(pred,y_test):
        if p == 0 and y == 0:
            true_negatives += 1
        elif p == 0 and y == 1:
            false_negatives += 1
        elif p == 1 and y == 0:
            false_positives += 1
        elif p == 1 and y == 1:
            true_positives += 1
        else:
            print("O_o") #Just in case
    
    
    total_predictions = total_predictions + true_negatives + false_negatives + false_positives + true_positives
    accuracy = 1.0*(true_positives + true_negatives)/total_predictions
    if true_positives+false_positives == 0:
        precision = 0
    else:
        precision = 1.0*true_positives/(true_positives+false_positives)
    recall = 1.0*true_positives/(true_positives+false_negatives)
    f1 = 2.0 * true_positives/(2*true_positives + false_positives+false_negatives)
    if (4*precision + recall) == 0:
        f2 = 0
    else:
        f2 = (1+2.0*2.0) * precision*recall/(4*precision + recall)
        
        
    PERF_FORMAT_STRING = "\
    \tAccuracy: {:>0.{display_precision}f}\tPrecision: {:>0.{display_precision}f}\t\
    Recall: {:>0.{display_precision}f}\tF1: {:>0.{display_precision}f}\tF2: {:>0.{display_precision}f}"
    
    print('Best estimator:/n', clf.best_estimator_ )
    print(PERF_FORMAT_STRING.format(accuracy, precision, recall, f1, f2, display_precision = 5))
    
    
    joblib.dump(clf, './classifier/forest.pkl') 
    
    clf = None
    
    svc = LinearSVC()
    
    svc.fit(X_train,y_train)
    print(svc.score(X_test, y_test))
    
    joblib.dump(svc, './classifier/svc.pkl') 
    
    return clf

    

if __name__ == "__main__":
    _ = train_classifier()    











